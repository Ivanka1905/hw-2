#!/bin/bash

watch_dir=~/watch

while true; do
	for file in "$watch_dir"/*; do
		if [ -f "$file" ] then 
			echo "File contents: "
			cat "$file"
			mv "$file" "${file}.back"
		fi
	done
	sleep 1
done

