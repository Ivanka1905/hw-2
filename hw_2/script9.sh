#!/bin/bash

read -p "Write a file name: " NAME

if [ -f "$NAME" ]; then
	cat "$NAME"
else
	echo "Error! File "$NAME" doesn't exist"
fi
